import axios from "axios";

const getTrivia = async (number) => {
  const randomTrivia = await axios("http://numbersapi.com/" + number);

  return randomTrivia.data;
};

export default getTrivia;
