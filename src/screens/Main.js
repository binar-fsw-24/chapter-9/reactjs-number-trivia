import React, { useEffect, useState } from "react";
import getTrivia from "../utils/getTrivia";

export default function Main() {
  const [trivia, setTrivia] = useState("");
  useEffect(() => {
    const generateTrivia = async () => {
      const todayDate = new Date().getDate();
      const randomTrivia = await getTrivia(todayDate);

      setTrivia(randomTrivia);
    };
    
    generateTrivia();
  }, []);
  return (
    <div>
      <h2>Today's trivia</h2>
      <p>{trivia}</p>
    </div>
  );
}
