import axios from "axios";
import React, { useEffect, useState } from "react";

export default function DetailGame() {
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    if (counter === 3) {
      hitApi();
    }
  }, [counter])

  const handlerClick = () => {
    if (counter < 3) {
      // hitApi();
      setCounter(counter + 1);
    }

  };

  const hitApi = async () => {
    const api = await axios("http://localhost:3001/", {
      headers: {
        Authorization: localStorage.getItem("accessToken"),
      },
    });

    console.log(api);
  };
  return (
    <div>
      Sudah main {counter} x <button onClick={handlerClick}>Main 1x</button>
    </div>
  );
}
