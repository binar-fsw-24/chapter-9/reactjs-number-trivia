import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Profile() {
  const [profile, setProfile] = useState({});
  const navigate = useNavigate();

  useEffect(() => {
    const getProfile = async () => {
      try {
        const profileFetch = await axios("http://localhost:3001/", {
          headers: {
            Authorization: localStorage.getItem("accessToken"),
          },
        });
        setProfile(profileFetch.data);
        console.log("PROFILE>>", profile);
      } catch (error) {
        if (error.response.status === 401) {
          navigate("/login");
        }
      }
    };
    getProfile();
  }, []);

  return (
    <div>
      <p>{profile.nama}</p>
      <p>{profile.email}</p>
    </div>
  );
}
