import React, { useState } from "react";
import getTrivia from "../utils/getTrivia";

export default function NumberTrivia() {
  const [selectedNumber, setSelectedNumber] = useState("");
  const [trivia, setTrivia] = useState("");

  const generateTrivia = async () => {
    const getSelectedTrivia = await getTrivia(selectedNumber);

    setTrivia(getSelectedTrivia);
  };

  return (
    <div>
      <input
        type='number'
        value={selectedNumber}
        onChange={(e) => setSelectedNumber(e.target.value)}
      />
      <button onClick={generateTrivia}>Submit</button>
      <p>{trivia}</p>
    </div>
  );
}
