import React from "react";
import { useNavigate } from "react-router-dom";

export default function Game() {
  const navigate = useNavigate();
  const navigationHandler = () => {
    if (!localStorage.getItem("accessToken")) {
      navigate("/login");
      return;
    }
    navigate("/game-detail");
  };
  return (
    <div>
      Game
      <button onClick={navigationHandler}>Detail</button>
    </div>
  );
}
