import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const loginHandler = async () => {
    const login = await axios.post("http://localhost:3001/login", {
      username,
      password,
    });

    localStorage.setItem("accessToken", login.data.accessToken);

    navigate("/profile");
  };

  return (
    <div>
      <input value={username} onChange={(e) => setUsername(e.target.value)} />{" "}
      <br />
      <input
        type='password'
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />{" "}
      <br />
      <button onClick={loginHandler}>Submit</button>
    </div>
  );
}
