import { Link, Route, Routes } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";

import Main from "./screens/Main";
import NumberTrivia from "./screens/NumberTrivia";
import Login from "./screens/Login";
import Profile from "./screens/Profile";
import Game from "./screens/Game";
import DetailGame from "./screens/DetailGame";

function App() {
  return (
    <div className='container'>
      <header>
        <h1>Number Trivia with ReactJS</h1>
        <Link to='/'>Main</Link> <Link to='/trivia'>Number Trivia</Link>{" "}
        <Link to='/login'>Login</Link> <Link to='/profile'>Profile</Link>{" "}
        <Link to='/game'>Game</Link>{" "}
      </header>
      <Routes>
        <Route path='/' element={<Main />} />
        <Route path='/trivia' element={<NumberTrivia />} />
        <Route path='/login' element={<Login />} />
        <Route path='/profile' element={<Profile />} />
        <Route path='/game' element={<Game />} />
        <Route path='/game-detail' element={<DetailGame />} />
      </Routes>
    </div>
  );
}

export default App;
